// let bamModule = function(str){
module.exports = function(str){
    let mod, elem;

    let elementDividers = [
        '—',
        '__',
        '___',
    ]

    let modDividers = [
        '–',
        '_',
        '--',
    ]

    elementDividers.forEach((item)=>{
        let arr = str.split(item)

        if(arr.length>1){
            elem = item
        }
    })

    let flag = false;
    modDividers.forEach((item)=>{
        let arr = str.split(item).filter((arrItem)=>arrItem!=='')

        if(arr.length>1&&!flag){
            mod = item
            flag=true
        }
    })

    return {
        mod,
        elem,
    }
}


