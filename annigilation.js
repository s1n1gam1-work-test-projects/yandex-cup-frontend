var findLatestWeight = function(weights, i = weights.length - 1) {  
    if (i === 0) return weights[0];  
   
    weights.sort((a, b) => a - b);  

    weights[i - 1] = (weights[i] === weights[i-1]) ? 0 : weights[i] - weights[i-1];  
   
    return findLatestWeight(weights, i - 1);  
}

console.time('findLatestWeight')
console.log(findLatestWeight([2,7,4,1,8,1]))
console.timeEnd('findLatestWeight')